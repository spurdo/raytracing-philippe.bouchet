#ifndef RAY_CAST_H
#define RAY_CAST_H

#include "image.h"
#include "scene.h"
#include "ray.h"

struct ray image_cast_ray(const struct rgb_image *image,
                          const struct scene *scene, double x, double y);
double scene_intersect_ray(struct object_intersection *closest_intersection,
                           struct scene *scene, struct ray *ray);

#endif /* !RAY_CAST_H */
