#include <err.h>
#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "bmp.h"
#include "camera.h"
#include "image.h"
#include "normal_material.h"
#include "obj_loader.h"
#include "phong_material.h"
#include "scene.h"
#include "sphere.h"
#include "triangle.h"
#include "vec3.h"
#include "color.h"
#include "ray_cast.h"


static void build_test_scene(struct scene *scene, double aspect_ratio)
{
    // create a sample red material
    struct phong_material *red_material = zalloc(sizeof(*red_material));
    phong_material_init(red_material);
    red_material->surface_color = light_from_rgb_color(191, 32, 32);
    red_material->diffuse_Kn = 0.2;
    red_material->spec_n = 10;
    red_material->spec_Ks = 0.2;
    red_material->ambient_intensity = 0.1;

    // create a single sphere with the above material, and add it to the scene
    struct sphere *sample_sphere
        = sphere_create((struct vec3){0, 10, 0}, 4, &red_material->base);
    object_vect_push(&scene->objects, &sample_sphere->base);

    // go the same with a triangle
    // points are listed counter-clockwise
    //     a
    //    /|
    //   / |
    //  b--c
    struct vec3 points[3] = {
        {6, 10, 1}, // a
        {5, 10, 0}, // b
        {6, 10, 0}, // c
    };

    struct triangle *sample_triangle
        = triangle_create(points, &red_material->base);
    object_vect_push(&scene->objects, &sample_triangle->base);

    // setup the scene lighting
    scene->light_intensity = 5;
    scene->light_color = light_from_rgb_color(255, 255, 0); // yellow
    scene->light_direction = (struct vec3){-1, 1, -1};
    vec3_normalize(&scene->light_direction);

    // setup the camera
    double cam_width = 10;
    double cam_height = cam_width / aspect_ratio;

    scene->camera = (struct camera){
        .center = {0, 0, 0},
        .forward = {0, 1, 0},
        .up = {0, 0, 1},
        .width = cam_width,
        .height = cam_height,
        .focal_distance = focal_distance_from_fov(cam_width, 80),
    };

    // release the reference to the material
    material_put(&red_material->base);
}

static void build_obj_scene(struct scene *scene, double aspect_ratio)
{
    // setup the scene lighting
    scene->light_intensity = 5;
    scene->light_color = light_from_rgb_color(255, 255, 0); // yellow
    scene->light_direction = (struct vec3){-1, -1, -1};
    vec3_normalize(&scene->light_direction);

    // setup the camera
    double cam_width = 2;
    double cam_height = cam_width / aspect_ratio;

    // for some reason the object points in the z axis,
    // with its up on y
    scene->camera = (struct camera){
        .center = {0, 1, 2},
        .forward = {0, -1, -2},
        .up = {0, 1, 0},
        .width = cam_width,
        .height = cam_height,
        .focal_distance = focal_distance_from_fov(cam_width, 40),
    };

    vec3_normalize(&scene->camera.forward);
    vec3_normalize(&scene->camera.up);
}

typedef void (*render_mode_f)(struct rgb_image *, struct scene *, size_t x,
                              size_t y);

/* For all the pixels of the image, try to find the closest object
** intersecting the camera ray. If an object is found, shade the pixel to
** find its color.
*/
static void render_shaded(struct rgb_image *image, struct scene *scene,
                          size_t x, size_t y)
{   struct ray rays[4];

    rays[0] = image_cast_ray(image, scene, x, y);
    rays[1] = image_cast_ray(image, scene, x, (double) y + .5);
    rays[2] = image_cast_ray(image, scene, (double) x + .5, y);
    rays[3] = image_cast_ray(image, scene, (double) x + .5, (double) y + .5);

    struct object_intersection closest_intersection;

    struct vec3 pix_color;

    for (size_t i = 0; i < 4; i++)
    {
        double closest_intersection_dist
            = scene_intersect_ray(&closest_intersection, scene, &rays[i]);

        // if the intersection distance is infinite, do not shade the pixel
        if (isinf(closest_intersection_dist))
            continue;

        struct material *mat = closest_intersection.material;

        if (i == 0)
        {
            pix_color = mat->shade(mat, &closest_intersection.location,
                                   scene, &rays[i], 0);
        }

        else
        {
            struct vec3 mat_vec =
                mat->shade(mat,&closest_intersection.location,
                           scene, &rays[i], 0);

            pix_color = vec3_add(&pix_color, &mat_vec);
        }
    }

    pix_color = vec3_mul(&pix_color, .25);
    rgb_image_set(image, x, y, rgb_color_from_light(&pix_color));
}

/* For all the pixels of the image, try to find the closest object
** intersecting the camera ray. If an object is found, shade the pixel to
** find its color.
*/
static void render_normals(struct rgb_image *image, struct scene *scene,
                           size_t x, size_t y)
{
    struct ray ray = image_cast_ray(image, scene, x, y);

    struct object_intersection closest_intersection;
    double closest_intersection_dist
        = scene_intersect_ray(&closest_intersection, scene, &ray);

    // if the intersection distance is infinite, do not shade the pixel
    if (isinf(closest_intersection_dist))
        return;

    struct material *mat = closest_intersection.material;
    struct vec3 pix_color = normal_material.shade(
        mat, &closest_intersection.location, scene, &ray, 0);
    rgb_image_set(image, x, y, rgb_color_from_light(&pix_color));
}

/* For all the pixels of the image, try to find the closest object
** intersecting the camera ray. If an object is found, shade the pixel to
** find its color.
*/
static void render_distances(struct rgb_image *image, struct scene *scene,
                             size_t x, size_t y)
{
    struct ray ray = image_cast_ray(image, scene, x, y);

    struct object_intersection closest_intersection;
    double closest_intersection_dist
        = scene_intersect_ray(&closest_intersection, scene, &ray);

    // if the intersection distance is infinite, do not shade the pixel
    if (isinf(closest_intersection_dist))
        return;

    assert(closest_intersection_dist > 0);

    // distance from 0 to +inf
    // we want something from 0 to 1
    double depth_repr = 1 / (closest_intersection_dist + 1);
    uint8_t depth_intensity = depth_repr * 255;
    struct rgb_pixel pix_color
        = {depth_intensity, depth_intensity, depth_intensity};
    rgb_image_set(image, x, y, pix_color);
}


struct data
{
    int y;
    int lines;

    struct rgb_image *image;
    struct scene scene;

    render_mode_f renderer;
};

/*
** Thread routine to render pixels.
*/
static void *thread_routine(void *d)
{
    struct data *data = d;

    int y = data->y;
    int lines = data->lines;

    struct rgb_image *image = data->image;
    struct scene scene = data->scene;

    render_mode_f renderer = data->renderer;

    for (; y < lines; y++)
        for (size_t x = 0; x < image->width; x++)
            renderer(image, &scene, x, y);

    return NULL;
}


/* Render every pixel, with use of threads in order
** to speed up the process.
*/
static void multithread_render(struct rgb_image *image,
                               struct scene scene,
                               render_mode_f renderer)
{
    // Number of threads to launch per clusters
    // of lines.
    int threads_per_line = sysconf(_SC_NPROCESSORS_ONLN);

    // Number of lines to be rendered per thread
    int lines = image->height / threads_per_line;

    // Initializing list of data for threads
    struct data data[threads_per_line];

    data[0].y = 0;
    data[0].lines = lines;
    data[0].image = image;
    data[0].scene = scene;
    data[0].renderer = renderer;

    // Iintialize each data's designated
    // segment to treat in the image.
    for (int i = 1; i < threads_per_line; i++)
    {
        data[i] = data[i - 1];

        data[i].y += lines;
        data[i].lines += lines;
    }

    pthread_t thread[threads_per_line];

    for (int i = 0; i < threads_per_line; i++)
        pthread_create(&thread[i], NULL, thread_routine, (void *) &data[i]);

    for (int i = 0; i < threads_per_line; i++)
        pthread_join(thread[i], NULL);

}

int main(int argc, char *argv[])
{
    int rc;

    if (argc < 3)
        errx(1, "Usage: SCENE.obj OUTPUT.bmp [--normals] [--distances]");

    struct scene scene;
    scene_init(&scene);

    // initialize the frame buffer (the buffer that will store the result of the
    // rendering)
    struct rgb_image *image = rgb_image_alloc(1000, 1000);

    // set all the pixels of the image to black
    struct rgb_pixel bg_color = {0};
    rgb_image_clear(image, &bg_color);

    double aspect_ratio = (double)image->width / image->height;

    // build the scene
    build_obj_scene(&scene, aspect_ratio);

    if (load_obj(&scene, argv[1]))
        return 41;

    // parse options
    render_mode_f renderer = render_shaded;
    for (int i = 3; i < argc; i++)
    {
        if (strcmp(argv[i], "--normals") == 0)
            renderer = render_normals;
        else if (strcmp(argv[i], "--distances") == 0)
            renderer = render_distances;
    }

    // render all pixels
    multithread_render(image, scene, renderer);

    // write the rendered image to a bmp file
    FILE *fp = fopen(argv[2], "w");
    if (fp == NULL)
        err(1, "failed to open the output file");

    rc = bmp_write(image, ppm_from_ppi(80), fp);
    fclose(fp);

    // release resources
    scene_destroy(&scene);
    free(image);
    return rc;
}
